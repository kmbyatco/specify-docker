#!/bin/bash
user=root
password=password
database=SpecifyDB
specify_it=ITUser
specify_it_password=ITPassword
specify_master=MasterUser
specify_master_password=MasterPassword

#https://stackoverflow.com/questions/25503412/how-do-i-know-when-my-docker-mysql-container-is-up-and-mysql-is-ready-for-taking
#http://manpages.ubuntu.com/manpages/xenial/man1/mysqladmin.1.html
while ! mysqladmin ping -hdb --silent; do
    sleep 1
done

mysql --user="$user" --password="$password" --host="db" --execute="CREATE DATABASE $database; GRANT SELECT, INSERT, UPDATE, DELETE, LOCK TABLES, ALTER, CREATE, DROP, INDEX, GRANT OPTION, REFERENCES ON $database.* TO '$specify_it'@'%' IDENTIFIED BY \"$specify_it_password\"; GRANT RELOAD ON *.* TO '$specify_it'@'%' IDENTIFIED BY \"$specify_it_password\"; GRANT SELECT, INSERT, UPDATE, DELETE, LOCK TABLES, CREATE, ALTER ON $database.* TO '$specify_master'@'%' IDENTIFIED BY \"$specify_master_password\";"

#https://serverfault.com/questions/80862/bash-script-repeat-command-if-it-returns-an-error
until /usr/bin/xdg-open /root/specify6; do
	echo Trying XDG-OPEN again...
	sleep 1
done