#!/bin/bash

#https://stackoverflow.com/questions/25503412/how-do-i-know-when-my-docker-mysql-container-is-up-and-mysql-is-ready-for-taking
while ! mysqladmin ping -hdb --silent; do
    sleep 1
done

#https://github.com/specify/specify7#make-django_migrations
cp /specify7/specifyweb/settings/local_specify_settings.py{,.bak}

sed -i 's/MasterUser/root/g' /specify7/specifyweb/settings/local_specify_settings.py
sed -i 's/MasterPassword/password/g' /specify7/specifyweb/settings/local_specify_settings.py

mv /specify7/specifyweb/settings/local_specify_settings.py{.bak,}

# tail -f /dev/null

virtualenv /specify7/ve && \
    /bin/bash -c "source /specify7/ve/bin/activate" && \
	make all -C specify7

python /specify7/manage.py runserver 0.0.0.0:8000
# /usr/sbin/apache2ctl -D FOREGROUND