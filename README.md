Installing Specify 6 and 7 on any computer using Docker
=======================================================
The [Specify](https://github.com/specify) programs are usually installed via command line in a UNIX environment, however, not all computers have a workable UNIX system. Also, due to unclear documentation, there might be a need for trial-and-error to end up with a working Specify installation. 

Doing reinstalls on a system might lead to problems when the OS is not fully cleared of old Specify programs and dependencies. Using [Docker](https://www.docker.com) ensures that we could tweak and reinstall our Specify programs every time without ruining our computers.

# 1. Downloading Docker and installing for your computer
For a working installation of Specify via Docker, you will be needing:

* [Docker-CE edition](https://www.docker.com/community-edition)
* [docker-compose](https://docs.docker.com/compose/install/)

Sadly, there might be a need to create an account nowadays for a docker installer. So, after selecting an appropriate download for your OS, create an account when prompted.

# 2. Prepare the Specify docker files
The docker files needed to install Specify is maintained here (https://gitlab.com/kmbyatco/specify-docker). You can download all the latest scripts as a [zip file](https://gitlab.com/kmbyatco/specify-docker/-/archive/master/specify-docker-master.zip), but it is recommended to keep up-to-date using `git clone` 

```bash
    git clone https://gitlab.com/kmbyatco/specify-docker.git
```

# 3. Using Docker with the default Specify build scripts
Open a terminal, then navigate to the downloaded docker files. Run the `docker-compose build` command.

```bash
    cd specify-docker
    docker-compose build
```

During the process, docker will build a local copy of both Specify 6 and Specify 7 to your computers, complete with required dependencies, such as Java, Python, Apache and MySQL. These dependencies will not conflict with similar existing versions on your computer. 

The docker installation steps should mirror the [recommended installation steps](https://github.com/specify/specify7#ordinary-deployment) found at the Specify 7 documentation.

# 4. Running Specify with Docker

After building, run:

```bash
    docker-compose up
```

to activate the Specify applications. This will create `specify6_data` and `mysql_data` folders on the repo directory, which will store data so entries in the database will persist, even after stopping the program. 

Terminate the programs by pressing

```bash
    <control>+C
```

at the terminal window running `docker-compose up`

## 4.1 Specify 6
Specify 6 can be accessed by a virtual desktop in a browser at http://localhost:8080, and then selecting **vnc_auto.html** at the directory listing. 

### 4.1.1 Local Specify 6 database creation
If the program is going to connect to a remote MySQL database for Specify, there is no need to do this step.

But, if a local database is desired, it can be generated using the `SpWizard` application found at the virtual desktop. An empty Specify MySQL database is needed in running `SpWizard`. To create this, first, terminate the running `docker-compose`, then, run the alternative docker-compose file

```bash
    <control>+C
    docker-compose -f docker-compose-createSp6Database.yml up
```
Be careful in running this command repeatedly, as each invocation will create an empty Specify database, deleting the previous one. 

To check if the creation is successful, see if the Specify 6 applications can be seen again on the virtual desktop. If they can be seen, the MySQL database has been created and is saved on the local `mysql_data` folder. You can now terminate the running alternative `docker-compose`, and rerun the default docker-compose file

```bash
    <control>+C
    docker-compose up
```

The database has the following credentials:

Specify MySQL Variables | Values
--- | ---
MySQL root user | root
MySQL root password | password
Specify database | SpecifyDB
Specify IT user | ITUser
Specify IT user password | ITPassword
Specify master user | MasterUser
Specify master user password | MasterPassword

Follow the instructions given on the `SpWizard` application, and provide the approrpiate values from the table when prompted.

## 4.2 Specify 7

### 4.2.1 Before Specify 6 database creation
The default Specify 7 dockerfile will not run without a locally created Specify database. This can be seen with errors from the `docker-compose up` logs such as:

```bash
db_1        | 2018-07-25 16:51:48 9 [Warning] Access denied for user 'MasterUser'@'172.18.0.5' (using password: YES)
specify7_1  | django.db.utils.OperationalError: (1045, "Access denied for user 'MasterUser'@'172.18.0.5' (using password: YES)")
```

You can modify the Specify 7 dockerfile so it will link to a remote MySQL database, however, it might be easier to generate a [local specify database](https://gitlab.com/kmbyatco/specify-docker#411-local-specify-6-database-creation) as it will link to it automatically.

### 4.2.2 After Specify 6 database creation
Specify 7 takes a while to get ready. If you’ll notice the build logs from `docker-compose up`, it will have entries that say that it's still building its database:

```bash
specify7_1  | New python executable in /specify7/ve/bin/python
specify7_1  | Installing setuptools, pip, wheel...done.
specify7_1  | make: Entering directory '/specify7'
specify7_1  | /usr/bin/pip install --upgrade -r requirements.txt
specify7_1  | Requirement already up-to-date: Django==1.10.2 in /usr/local/lib/python2.7/dist-packages (from -r requirements.txt (line 1))
specify7_1  | Requirement already up-to-date: mysqlclient==1.3.7 in /usr/local/lib/python2.7/dist-packages (from -r requirements.txt (line 2))
specify7_1  | Requirement already up-to-date: SQLAlchemy==1.0.3 in /usr/local/lib/python2.7/dist-packages (from -r requirements.txt (line 3))
specify7_1  | Requirement already up-to-date: requests==2.2.1 in /usr/local/lib/python2.7/dist-packages (from -r requirements.txt (line 4))
specify7_1  | Requirement already up-to-date: pycrypto==2.4.1 in /usr/local/lib/python2.7/dist-packages (from -r requirements.txt (line 5))
```

That means, until it builds its code, Specify 7 isn't ready to be accessed. This is a quirk on Specify 7 usage with docker, as everytime we do `docker-compose up`, we are compiling Specify 7 to merge with the default MySQL database using online Python libraries. With that, to use Specify 7 on docker, we will need an **active internet connection**. 

Also, by default, the dockerized Specify 7 installation will be using the built-in development server.

To check if Specify 7 is ready to use, check if the database migration step is finished. This can be seen if the logs show:

```bash
specify7_1  | Running migrations:
specify7_1  |   No migrations to apply.
specify7_1  | make: Leaving directory '/specify7'
```

When finished, Specify 7 can be accessed via web browser at http://localhost:8000